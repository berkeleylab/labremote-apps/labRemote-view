from setuptools import setup, find_packages

setup(
    name = "labRemote-view",
    version = "0.1",
    description = "",
    long_description = "",
    url = "",
    author = "Daniel Joseph Antrim",
    author_email = "daniel.joseph.antrim@cern.ch",
    package_dir = { "": "src/" },
    packages = find_packages(where = "src/"),
    include_package_data = True,
    python_requires = ">=3.6",
    install_requires = [
        "Click>=6.0",
        "numpy",
        "matplotlib",
        "scipy",
        "jsonschema",
        "PySide2"
    ],
    entry_points = {"console_scripts": ["lr=labremote_view.cli:cli"]}
)
