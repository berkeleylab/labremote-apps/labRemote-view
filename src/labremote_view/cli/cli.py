import click

from . import config_gen

@click.group(context_settings = dict(help_option_names = ["-h", "--help"]))
def labremote_view() :
    """Top-level entrypoint into labRemote-view infrastructure"""

labremote_view.add_command(config_gen.config_gen)
