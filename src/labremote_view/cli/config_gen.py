import click

from pathlib import Path
import sys, os

import labRemote
import logging
logger = logging.getLogger(__name__)

@click.group(name = "config_gen")
def cli():
    """The labRemote config generation CLI group."""


@cli.command()
@click.option(
    "--no-gui",
    is_flag = True)
def config_gen(no_gui) :

    logger.info(f"Config generator: no-gui = {no_gui}")
