""" labRemote-view command line interface """

from . cli import labremote_view as cli

__all__ = ["cli", "config_gen"]
